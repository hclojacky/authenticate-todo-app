import { table, getMinifiedRecord } from './utils/Airtable';
import OwnsRecord from './middleware/OwnsRecord';

export default OwnsRecord(async (req, res) => {
  const { id, fields } = req.body;

  try {
    const updatedRecords = await table.update([{ id, fields }]);
    res.statusCode = 200;
    res.json(getMinifiedRecord(updatedRecords[0]));
  } catch (error) {
    res.statusCode = 500;
    res.json({ msg: 'Something went wrong' });
  }
});
